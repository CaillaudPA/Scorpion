#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#Pierre Antoine & Alexandre Chollet

from math import pow, sqrt, sin, degrees, radians
import random
import matplotlib.pyplot as plt

# axes
axes = plt.gca()
#axes.set_ylim(0, 600)


'''
angle = 45 #angle
lb = 10 #longueur du bras de l'arc (en m)
b = 0.5 #base de la section du bras (en m)
h = 0.5 #hauteur de la section du bras (en m)
lc = 1 #longueur de la corde (en m)
lf = 1 #longueur de la fleche (en m)
"lf": random.randint(0,200)	# longueur de la fleche

lv = sqrt(pow(individu["lb"],2)-(1/4)*pow(individu["lc"],2)) #Longueur à vide (en m)
ld = individu["lf"] - lv #Longueur du déplacement (en m)
mp = mv*individu["b"]*individu["h"]*individu["lf"] #Masse du projectile (en kg)
v = sqrt((k*pow(ld,2))/mp) #Vélocité V (en m/s)
p = (pow(v,2)/g)*degrees(sin(2*individu["α"])) #Portée P (en m)
		
'''

# Distance de la cible 
targetDistance = 200.0

#Aluminium
mv = 2700.0 #masse volumique de la flèche mv (en kg/m3)
E = 62.0 #module de Young du matériau de l’arc E (en GPa)
V = 0.24 #Coefficient de Poisson du matériau de l’arc ν
#Gravite
g = 9.81
k = (1.0/3.0)*(E/(1.0-2.0*V)) #Ressort K (en N/m)

# Materiaux
lstMateriaux = [
	{"nom": "Acier", "mv": 7850, "e": 210, "V": 0.33},
	{"nom": "Bois", "mv": 800, "e": 11, "V": 0.4},
	{"nom": "Caoutchou", "mv": 1000, "e": 0.2, "V": 0.2},
]

#Genere un gene aleatoire
def getNewRandomGene(gene):
	if(gene == "angle"):
		return random.uniform(0.0,90.0)
	elif (gene == "lb"):
		return random.uniform(0.0,5.0)
	elif (gene == "b"):
		return random.uniform(0.0,5.0)
	elif (gene == "lc"):
		return random.uniform(0.0,5.0)
	elif (gene == "lf"):
		return random.uniform(0.1,2.0)
	elif (gene == "bf"):
		return random.uniform(0.0,0.1)
	elif (gene == "hf"):
		return random.uniform(0.0,0.1)
	else:
		return 99999999999999999999999

#Generation des individu
def generationIndividu(n, nbMateriel):
	panel = []
	for i in range(0,n):
		individual = {
			"angle": random.uniform(0.0,90.0), # angle [0]
			"lb": random.uniform(0.0,5.0), # longueur bras [1]
			"b": random.uniform(0.0,5.0), # base de la section du bras [2]
			"h": random.uniform(0.0,5.0), # hauteur de la section du bras [3]
			"lc": random.uniform(0.0,5.0), # longueur de la corde [4]
			"lf": random.uniform(0.1,2.0),	# longueur de la fleche [5]
			"materiel": nbMateriel, # [6]
			"bf": random.uniform(0.0,0.1),	# base de la fleche [7]
			"hf": random.uniform(0.0,0.1),	# hauteur de la fleche [8]
			"uuid": i
		}
		panel.append(individual)
	return panel

#Calcul de la distance de tir
def calculDistanceTir(individual):
	
	if(individual["lc"] > individual["b"]):
		return 0
	k = (1.0/3.0)*(lstMateriaux[individual["materiel"]]["e"]/(1.0-2.0*lstMateriaux[individual["materiel"]]["V"])) #Ressort K (en N/m)
	try:
		lv = 0.5*sqrt(pow(individual["lb"],2)-pow(individual["lc"],2)) #Longueur à vide (en m)
		if(lv > individual["lf"]):
			return 0
	except ValueError:
		return 0

	ld = individual["lf"] - lv #Longueur du déplacement (en m)
	mp = lstMateriaux[individual["materiel"]]["mv"]*individual["bf"]*individual["hf"]*individual["lf"] #Masse du projectile (en kg)
	try:
		v = sqrt((k*pow(ld,2))/mp) #Vélocité V (en m/s)
	except ZeroDivisionError:
		return 0
	except ValueError:
		return 0
	I = (individual["b"]*pow(individual["h"],3))/12.0
	F = k*ld
	f = (F*pow(individual["lb"], 3))/48.0*lstMateriaux[individual["materiel"]]["e"]*I

	if ld > f:
		return 0
	reach = (pow(v,2)/g)*sin(radians(2.0*individual["angle"]%360.0)) #Portée P (en m)
	
	return reach

#Autre fonction avec puissance, non utilise
def calculDistance(individu):

	#Erreur; limites 
	err = 1

	#La longueur de la corde ne doit pas etre plus longue que la longueur du bras de l'arbre
	if(individu["lc"] > individu["lb"]):
		err = 999999999999999999999999

	#Longueur à vide (en m)
	try:
		lv = 0.5*sqrt(pow(individu["lb"],2)-pow(individu["lc"],2))
	except ValueError:
		lv = 999999999999999999999999

	#La longueur a vide ne doit pas être plus grande que la longueur de la fleche
	if(lv > individu["lf"]):
		err = 999999999999999999999999

	# Longueur du déplacement (en m)
	ld = individu["lf"] - lv 
	# Masse du projectile (en kg)
	mp = mv*individu["b"]*individu["h"]*individu["lf"] 


	# Vélocité V (en m/s)
	try:
		v = sqrt((k*pow(ld,2))/mp) #Vélocité V (en m/s)
	except ZeroDivisionError:
		v = 999999999999999999999999
	except ValueError:
		v = 999999999999999999999999

	#CALCUL DE LA PORTEE ET DE LENERGIE

	# Portée P (en m)
	portee = (pow(v,2)/g)*degrees(sin(radians(2*individu["angle"]))) #Portée P (en m)
	
	

	# Energie d'impact en joule
	energie = 0.5*mp*pow(v, 2)

	return [portee, energie]

#Fonction de fitness
def calculFitness(lstIndividu):
	fitnessedPanel = []

	for i in range(0,len(lstIndividu)):

		portee = calculDistanceTir(lstIndividu[i])

		weight = (1.0/abs(portee - targetDistance))*10000
		
		array = {
			"weight": weight,
			"identifiant": i,
			"portee":portee,
		}
		fitnessedPanel.append(array)

	return fitnessedPanel

#Selection naturel, methode pierre-antoine caillaud
def selectionNaturel(lstIndividu, lstScore):
	lstChildren = []

	lstSelectedByWeight = sorted(lstScore, key=lambda dct: dct['weight'], reverse=True)		
	
	lstSelectedByWeightPuissance = sorted(lstScore, key=lambda dct: dct['weight_puissance'], reverse=True)	

	lstCopie = lstIndividu
	
	lstReproducteur = []

	lst_gene = ["angle","lb","b","h","lc","lf"]

	for x in xrange(0,50):
		lstReproducteur.append(lstIndividu[lstSelectedByWeight[x]["identifiant"]])		

	x = 0
	while len(lstReproducteur) < 100:

		if lstSelectedByWeightPuissance[x] not in lstReproducteur:
			lstReproducteur.append(lstIndividu[lstSelectedByWeightPuissance[x]["identifiant"]])

		x = x + 1

	while len(lstReproducteur) < 125:
		
		r = random.randint(0,1999)
		#print r
		if lstIndividu[r] not in lstReproducteur:
			lstReproducteur.append(lstIndividu[r])
	#print '#{#{#{#{#{#'

	while len(lstChildren) < 2000:
		random_parents = random.sample(xrange(0,49), 2)
		p1 = lstReproducteur[random_parents[0]]
		p2 = lstReproducteur[random_parents[1]]

		random.shuffle(lst_gene)

		new_child = {}
		#print len(lstChildren)
		if(random.randint(0,1) == 1):
			for i in xrange(0,len(lst_gene)):
				if i<3:
					new_child[lst_gene[i]] = p1[lst_gene[i]]
					#print lst_gene[i]
				else:
					new_child[lst_gene[i]] = p2[lst_gene[i]]
		else:
			nb_gene = random.randint(0,6)
			for i in xrange(0,len(lst_gene)):
				if(i<nb_gene):
					new_child[lst_gene[i]] = p1[lst_gene[i]]
				else:
					new_child[lst_gene[i]] = p2[lst_gene[i]]
		new_child['uuid'] = len(lstChildren)

		lstChildren.append(new_child)

	return lstChildren

#Selection Naturel, methode par rang
def selectionNaturelParRang(lstIndividu, lstScore):
	lstChildren = []

	lstSelectedByWeight = sorted(lstScore, key=lambda dct: dct['weight'], reverse=True)		

	roulette = []

	lst_gene = ["angle","lb","b","h","lc","lf", "bf", "hf"]

	i = 2000
	for x in xrange(0,2000):
		tmp = i
		for y in xrange(0,i):
			roulette.append(lstSelectedByWeight[x]['identifiant'])
		i-=1
		
	while len(lstChildren) < 2000:

		parent_different = False

		while parent_different != True:
			random_parents = random.sample(xrange(0,len(roulette)-1), 2)
		
			if random_parents[0] != random_parents[1] : 
				p1 = lstIndividu[roulette[random_parents[0]]]
				p2 = lstIndividu[roulette[random_parents[1]]]
				parent_different = True

		random.shuffle(lst_gene)

		new_child = {}
		#print len(lstChildren)
		if(random.randint(0,1) == 1):
			for i in xrange(0,len(lst_gene)):
				if(random.randint(0,100) == 1):
					new_child[lst_gene[i]] = getNewRandomGene(lst_gene[i])
				elif i<3:
					new_child[lst_gene[i]] = p1[lst_gene[i]]
				else:
					new_child[lst_gene[i]] = p2[lst_gene[i]]
		else:
			nb_gene = random.randint(0,6)
			for i in xrange(0,len(lst_gene)):
				if(random.randint(0,100) == 1):
					new_child[lst_gene[i]] = getNewRandomGene(lst_gene[i])
				elif(i<nb_gene):
					new_child[lst_gene[i]] = p1[lst_gene[i]]
				else:
					new_child[lst_gene[i]] = p2[lst_gene[i]]


		new_child['materiel'] = p1["materiel"]
		new_child['uuid'] = len(lstChildren)

		lstChildren.append(new_child)
	
	return lstChildren

#Calcul de la moyenne des distances		
def calculMoyenneDistance(lstScore):

	sommeDistance = 0.0

	for x in xrange(0,len(lstScore)-1):
		sommeDistance = sommeDistance + lstScore[x]["portee"]
		
	moyenne = sommeDistance / len(lstScore) 

	return moyenne

#Calcul de la moyenne des parametres		
def calculMoyenneParametres(lstIndividu):
	
	somme = {}
	somme["angle"] = 0.0
	somme["lb"] = 0.0
	somme["b"] = 0.0
	somme["h"] = 0.0
	somme["lc"] = 0.0
	somme["lf"] = 0.0
	somme["bf"] = 0.0
	somme["hf"] = 0.0

	for x in xrange(0, len(lstIndividu)-1):
		somme["angle"] += lstIndividu[x]["angle"] 
		somme["lb"] += lstIndividu[x]["lb"] 
		somme["b"] += lstIndividu[x]["b"] 
		somme["h"] += lstIndividu[x]["h"] 
		somme["lc"] += lstIndividu[x]["lc"] 
		somme["lf"] += lstIndividu[x]["lf"] 
		somme["bf"] += lstIndividu[x]["bf"] 
		somme["hf"] += lstIndividu[x]["hf"] 		
	
	somme["angle"] = somme["angle"] / len(lstIndividu)
	somme["lb"] = somme["lb"] / len(lstIndividu)
	somme["b"] = somme["b"] / len(lstIndividu)
	somme["h"] = somme["h"] / len(lstIndividu)
	somme["lc"] = somme["lc"] / len(lstIndividu)
	somme["lf"] = somme["lf"] / len(lstIndividu)
	somme["bf"] = somme["bf"] / len(lstIndividu)
	somme["hf"] = somme["hf"] / len(lstIndividu)

	return somme			

#MAIN PROGRAM

plt.xlabel("Generation")

#Programme main
for x in xrange(0,1):
	lstIndividu = generationIndividu(2000,0)

	lstScore = 0
	arrMoyDistance, arrMoyAngle, arrMoyLB, arrMoyB, arrMoyH, arrMoyLC, arrMoyLF, arrMoyBF, arrMoyHF  = ([] for i in range(9))

	print x
	#Pour chaque generation
	for y in xrange(0,300):
		print y
		lstScore = calculFitness(lstIndividu)
		arrMoyDistance.append(calculMoyenneDistance(lstScore))
		lstMoyParametres = calculMoyenneParametres(lstIndividu)

		arrMoyAngle.append(lstMoyParametres["angle"])
		arrMoyLB.append(lstMoyParametres["lb"])
		arrMoyB.append(lstMoyParametres["b"])
		arrMoyH.append(lstMoyParametres["h"])
		arrMoyLC.append(lstMoyParametres["lc"])
		arrMoyLF.append(lstMoyParametres["lf"])
		arrMoyBF.append(lstMoyParametres["bf"])
		arrMoyHF.append(lstMoyParametres["hf"])

		lstIndividu = selectionNaturelParRang(lstIndividu, lstScore)

	#Generation des graphiques
	plt.figure(1)
	plt.ylabel("Distance")
	plt.plot(arrMoyDistance)

	plt.figure(2)
	plt.ylabel("Angle de tir")
	plt.plot(arrMoyAngle)

	plt.figure(3)
	plt.ylabel("Longueur du bras de l'arc")
	plt.plot(arrMoyLB)

	plt.figure(4)
	plt.ylabel("Base de la section du bras")
	plt.plot(arrMoyB)

	plt.figure(5)
	plt.ylabel("Hauteur de la section du bras")
	plt.plot(arrMoyH)

	plt.figure(6)
	plt.ylabel("Longueur de la corde")
	plt.plot(arrMoyLC)

	plt.figure(7)
	plt.ylabel("Longueur de la fleche")
	plt.plot(arrMoyLF)

	plt.figure(8)
	plt.ylabel("Base de la fleche")
	plt.plot(arrMoyBF)

	plt.figure(9)
	plt.ylabel("Hauteur de la fleche")
	plt.plot(arrMoyHF)
	

plt.show()




